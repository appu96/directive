import {
   Directive,
   ElementRef,
   Renderer2,
   OnInit,
   HostListener,
   HostBinding,
   Input
    } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {
  @Input() defaultColor: string = 'transparent';
  @Input('appBetterHighlight') highlightColor: string = 'blue';
  @HostBinding('style.backgroundColor') backgroundColor: string;
  constructor(private eleRef: ElementRef, private renderer: Renderer2) {}
  ngOnInit() {
    this.backgroundColor = this.defaultColor;
   // this.renderer.setStyle(this.eleRef.nativeElement, 'backgroung-color', 'blue');
  }
  @HostListener('mouseenter') mouseover() {
    // ts-ignore
   // this.renderer.setStyle(this.eleRef.nativeElement, 'background-color', 'green');
   // this.backgroundColor = 'blue';
   this.backgroundColor = this.defaultColor;
  }
  @HostListener('mouseleave') mouseleave() {
   // this.renderer.setStyle(this.eleRef.nativeElement, 'background-color', 'transparent');
   // this.backgroundColor = 'transparent';
   this.backgroundColor = this.highlightColor;
  }




}
